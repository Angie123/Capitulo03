/*
 * 3.11 (Clase Cuenta modificada) Modifique la clase Cuenta (figura 3.8) 
para proporcionar un método llamado retirar, que retire dinero de un objeto Cuenta. 
Asegúrese de que el monto a retirar no exceda el saldo de Cuenta. Si
lo hace, el saldo debe permanecer sin cambio y el método debe imprimir un mensaje 
que indique “El monto a retirar excede el saldo de la cuenta”. 
Modifique la clase PruebaCuenta (figura 3.9) para probar el método retirar
 */
package ejercicio01;

/**
 *
 * @author Angélica Reyes Arellano
 */
public class Cuenta {
  

    private double saldo; // variable de instancia que almacena el saldo
   
    // constructor  

    /**
     *
     * @param saldoInicial
     */
    public Cuenta(double saldoInicial) {
        // valida que saldoInicial sea mayor que 0.0; 
        // si no lo es, saldo se inicializa con el valor predeterminado 0.0
        if (saldoInicial > 0.0) {
            saldo = saldoInicial;
        }
    } // fin del constructor de Cuenta
    // abona (suma) un monto a la cuenta
    public void abonar(double monto) {
        saldo =  saldo + monto; // suma el monto al saldo 
    } // fin del método abonar
    // devuelve el saldo de la cuenta
    public double obtenerSaldo() {
        return saldo; // proporciona el valor de saldo al método que hizo la llamada
    } // fin del método obtenerSaldo
    //Se crea el metodo cargar para verificar las condiciones del problema planteado
    public String cargar(double monto) {
        String cadena = "";
        //Condicion que determina si existe saldo suficiente de la cuenta
        if (monto > saldo) {
            cadena = "El monto a retirar excede el saldo de la cuenta";
        } else if (monto <= saldo) {
            cadena = "El monto es suficiente";
            saldo = saldo - monto;
        }
        return cadena;
    }
}