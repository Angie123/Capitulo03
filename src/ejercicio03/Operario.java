/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio03;

/**
 *
 * @author HP 14
 */
public class Operario extends Empleados {
    private double horasLaborables;
    public  Operario() {
    }
    public Operario(double horasLaborables) {
        this.horasLaborables = horasLaborables;
    }
    public Operario(double horasLaborables, String Nombre) {
        super(Nombre);
        this.horasLaborables = horasLaborables;
    }
    public double getHorasLaborables() {
        return horasLaborables;
    }
    public void setHorasLaborables(double horasLaborables) {
        this.horasLaborables = horasLaborables;
    }
    public void calcularelsueldo(double costoHora){
        if (this.getHorasLaborables()>40) {
            super.setSueldo((this.horasLaborables-40)*(costoHora*2));
            super.setSueldo((40*costoHora)+super.getSueldo());
        }else
            super.setSueldo(this.horasLaborables*costoHora);
    
    
}
}
