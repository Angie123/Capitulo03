/*
 * Cree una clase llamada Empleado, que incluya tres variables de instancia: un primer
nombre (tipo String), un apellido paterno (tipo String) y un salario mensual (double). Su clase debe tener un constructor
que inicialice las tres variables de instancia. Proporcione un método establecer y un método obtener para cada
variable de instancia. Si el salario mensual no es positivo, no establezca su valor. Escriba una aplicación de prueba
llamada PruebaEmpleado, que demuestre las capacidades de la clase Empleado. Cree dos objetos Empleado y muestre el
salario anual de cada objeto. Después, proporcione a cada Empleado un aumento del 10% y muestre el salario anual
de cada Empleado otra vez
 */
package ejercicio03;

/**
 *
 * @author HP 14
 */
public abstract class Empleado {

   private String primerNombre;
   private String apellidoPaterno;
   private String numeroSeguroSocial;
   
   // constructor con tres argumentos
   public Empleado( String nombre, String apellido, String nss )
   {
      primerNombre = nombre;
      apellidoPaterno = apellido;
      numeroSeguroSocial = nss;
      
   } // fin del constructor de Empleado con tres argumentos
   // establece el primer nombre
   public void establecerPrimerNombre( String nombre )
   {
      primerNombre = nombre;  // deber�a validar
   } // fin del método establecerPrimerNombre
   // devuelve el primer nombre
   public String obtenerPrimerNombre()
   {
      return primerNombre;
   } // fin del método obtenerPrimerNombre
   // establece el apellido paterno
   public void establecerApellidoPaterno( String apellido )
   {
      apellidoPaterno = apellido;  // deber�a validar
   } // fin del m�todo establecerApellidoPaterno
   // devuelve el apellido paterno
   public String obtenerApellidoPaterno()
   {
      return apellidoPaterno;
   } // fin del método obtenerApellidoPaterno
   // establece el número de seguro social
   public void establecerNumeroSeguroSocial( String nss )
   {
      numeroSeguroSocial = nss; // deber�a validar
   } // fin del método establecerNumeroSeguroSocial
   // devuelve el número de seguro social
   public String obtenerNumeroSeguroSocial()
   {
      return numeroSeguroSocial;
   } // fin del método obtenerNumeroSeguroSocial
   
   @Override
   public String toString()
   {
      return String.format( "%s %s\nnumero de seguro social: %s", 
         obtenerPrimerNombre(), obtenerApellidoPaterno(), obtenerNumeroSeguroSocial() );
   } // fin del m�todo toString
   // m�todo abstracto sobrescrito por las subclases concretas
   public abstract double ingresos(); // 
}
