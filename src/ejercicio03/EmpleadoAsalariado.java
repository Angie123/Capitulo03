/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ejercicio03;

/**
 *
 * @author HP 14
 */
public final class EmpleadoAsalariado extends Empleado {

   private double salarioMensual;
   // constructor de cuatro argumentos
   public EmpleadoAsalariado( String nombre, String apellido, String nss, 
      double salario )
   {
      super( nombre, apellido, nss ); // los pasa al constructor de Empleado
      establecerSalarioMensual( salario ); // valida y almacena el salario
   } // fin del constructor de EmpleadoAsalariado con cuatro argumentos
   // establece el salario
   public void establecerSalarioMensual( double salario )
   {
      if (salario >= 0.0 )
         salarioMensual = salario;
      else
         throw new IllegalArgumentException(
            "El salario mensual debe ser >= 0.0" );
   } // fin del m�todo establecerSalarioSemanal
   // devuelve el salario
   public double obtenerSalarioMensual()
   {
      return salarioMensual;
   } // fin del método obtenerSalariMensual
   // calcula los ingresos; sobrescribe el m�todo abstracto ingresos en Empleado
   @Override
   public double ingresos()
   {
      return obtenerSalarioMensual();
   } // fin del m�todo ingresos
   // devuelve representaci�n String de un objeto EmpleadoAsalariado
   @Override
   public String toString()
   {
      return String.format( "empleado asalariado: %s\n%s: $%,.2f", 
         super.toString(), "salario semanal", obtenerSalarioMensual() );
   } // fin del método toString    
}
