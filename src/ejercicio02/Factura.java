/*
 *  Cree una clase llamada Factura que una ferretería podría utilizar para representar una
factura para un artículo vendido en la tienda. Una Factura debe incluir cuatro piezas de información como variables
de instancia: un número de pieza (tipo String), la descripción de la pieza (tipo String), la cantidad de artículos de ese
tipo que se van a comprar (tipo int) y el precio por artículo (double). Su clase debe tener un constructor que inicialice
las cuatro variables de instancia. Proporcione un método establecer y un método obtener para cada variable de
instancia. Además, proporcione un método llamado obtenerMontoFactura, que calcule el monto de la factura (es
decir, que multiplique la cantidad de artículos por el precio de cada uno) y después devuelva ese monto como un valor
double. Si la cantidad no es positiva, debe establecerse en 0. Si el precio por artículo no es positivo, debe establecerse
en 0.0. Escriba una aplicación de prueba llamada PruebaFactura, que demuestre las capacidades de la clase Factura
 */
package ejercicio02;

/**
 *
 * @author Angélica Reyes Arellano
 */
public class Factura {
    // Variables de instancia que almacenan datos
    private String Npieza;
    private String Dpieza;
    private int  cantidad;
    private double precio;
    private double total;
    //Final de las variables de instancias 
    // Creación del constructor
    public Factura(String npieza, String dpieza, int cant, double precio){
       this.Npieza=npieza;
       this.Dpieza=dpieza;
       this.cantidad=cant;
       this.precio=precio;
        
    }
    // Final del constructor
    
   // metodos Establecer y obtener 
    public String getNpieza() {
        return Npieza;
    }
    public void setNpieza(String Npieza) {
        this.Npieza = Npieza;
        //fin del metodo set y get (Npieza)
    }
    public String getDpieza() {
        return Dpieza;
    }
    public void setDpieza(String Dpieza) {
        this.Dpieza = Dpieza;
        //fin del metodo set y get (Npieza)
    }
    public int getCantidad() {
        return cantidad;
    }
    public void setCantidad(int cantidad) {
        this.cantidad = (cantidad<0)?0:cantidad;
        //si el un numeo negativo es ingresado se le asigna el valor de 0
        //fin del metodo set y get (cantidad)
    }
    public double getPrecio() {
        return precio;
    }
    public void setPrecio(double precio) {
        this.precio = (precio<0)?0.0:precio;
        //si el un numeo negativo e ingresado se le asigna el valor de 0.0
        //fin del metodo set y get (precio)
    }
    
    //Metodo para la obtencion del total de la factura
    public void obtenerMontoFactura(){
        
        System.out.printf("El total a pagar es:"+(getPrecio()*getCantidad()));//calculo del total a pagar de la compra 
        
    }
    @Override
    public String toString(){
        return  "Piezas:" +getNpieza()+"\n"+ 
               "Descripcion:"+getDpieza()+"\n"+ 
               "Cantidad:" +getCantidad()+"\n";
 
}
}  