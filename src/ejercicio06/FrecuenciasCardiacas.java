/*
 * Mientras se ejercita, puede usar un monitor de frecuencia
cardiaca para ver que su corazón permanezca dentro de un rango seguro sugerido por sus entrenadores y doctores.
De acuerdo con la Asociación Estadounidense del Corazón (AHA) (www.americanheart.org/presenter.
jhtml?identifier=4736), la fórmula para calcular su frecuencia cardiaca máxima en pulsos por minuto es 220
menos su edad en años. Su frecuencia cardiaca esperada tiene un rango que está entre el 50 y el 85% de su frecuencia
cardiaca máxima. [Nota: estas fórmulas son estimaciones proporcionadas por la AHA. Las frecuencias cardiacas
máxima y esperada pueden variar con base en la salud, condición física y sexo del individuo. Siempre debe consultar
un médico o a un profesional de la salud antes de empezar o modificar un programa de ejercicios]. Cree una
clase llamada FrecuenciasCardiacas. Los atributos de la clase deben incluir el primer nombre de la persona, su apellido
y fecha de nacimiento (la cual debe consistir de atributos independientes para el mes, día y año de nacimiento).
Su clase debe tener un constructor que reciba estos datos como parámetros. Para cada atributo debe proveer métodos
establecer y obtener. La clase también debe incluir un método que calcule y devuelva la edad de la persona (en años),
un método que calcule y devuelva la frecuencia cardiaca máxima de esa persona, y otro método que calcule y devuelva
la frecuencia cardiaca esperada de la persona. Escriba una aplicación de Java que pida la información de la persona,
cree una instancia de un objeto de la clase FrecuenciasCardiacas e imprima la información a partir de ese objeto
(incluyendo el primer nombre de la persona, su apellido y fecha de nacimiento), y que después calcule e imprima la
edad de la persona en (años), frecuencia cardiaca máxima y rango de frecuencia cardiaca esperada.
 */
package ejercicio06;

/**
 *
 * @author Angélica Reyes
 */
public class FrecuenciasCardiacas {
    private String nombre;
    private String apellido;
    private int dia;
    private int mes;
    private int año;
    private int añoactual;
    //constructor inicializado
    public FrecuenciasCardiacas(String nombre, String apellido, int año, int mes, int dia, int añoactual){
        this.nombre=nombre;
        this.apellido=apellido;
        this.año=año;
        this.dia=dia;
        this.mes=mes;
        this.añoactual=añoactual;
        
    }
// Metodos get y set de todos los atributos
    public String getNombre() {
        return nombre;
    }
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }
    public String getApellido() {
        return apellido;
    }
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }
    public int getDia() {
        return dia;
    }
    public void setDia(int dia) {
        this.dia = dia;
        
    }
    public int getMes() {
        return mes;
    }
    public void setMes(int mes) {
        this.mes = mes;
        
    }
    public int getAño() {
        return año;
    }
    public void setAño(int año) {
        this.año = año;
       
        
    }
    public int getAñoactual() {
        return añoactual;
    }
    public void setAñoactual(int añoactual) {
        this.añoactual = añoactual;
    }
    
    public void calculoedad (){
       
        System.out.println("La edad del paciente  es:"+(getAñoactual()-getAño())+ " años");
    }
    public void frecuenciacardiaca(){
        System.out.println("La frecuencia cardiaca esperada del paciente esta en el rango del 50 y 85%");
    }
    public void calculofrecuenciamaxima(){
        System.out.println("La frecuencia cardiaca maxima del paciente es:"+(220-(getAñoactual()-getAño())));
    }
     public String toString(){
        return  "Nombre del Paciente:"+getNombre()+"\n"+
                "Apellido del paciente  :"+getApellido()+"\n"+
                "La fecha de nacimiento del paciente es:" +getAño()+"/"+getMes()+"/"+getDia(); 
     }// fin del metodo toString
}
